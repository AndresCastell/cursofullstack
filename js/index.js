$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({interval:2000})
});
$('#contactoBtn').removeClass('btn-outline-success');
  $('#contactoBtn').addClass('btn-primary');
  $('#contactoBtn').prop('disabled', false);

  $('#contacto').on('show.bs.modal', function(e){
     console.log('el modal se está mostrando');
      
      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-primary');
      $('#contactoBtn').prop('disabled', true);
  });
  $('#contacto').on('shown.bs.modal', function(e){
     console.log('el modal se está mostró');
  });
  $('#contacto').on('hide.bs.modal', function(e){
     console.log('el modal está oculto');
  });
  $('#contacto').on('hidden.bs.modal', function(e){
     console.log('el modal se oculto');
      $('#contactoBtn').prop('disabled', false);
  });
    